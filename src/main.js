import Vue        from 'vue'
import App        from './App.vue'
import Loading    from './components/Loading.vue'
import Navigation from './components/Navigation.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
